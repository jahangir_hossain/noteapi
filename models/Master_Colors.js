const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const MasterColorSchema = new Schema({
    userId: {
        type: String
    },
    colors: [String]
    
});

module.exports = Master_Colors = mongoose.model("master_colors", MasterColorSchema);
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const MasterLabelSchema = new Schema({
    userId: {
        type: String
    },
    labels: [String]
    
});

module.exports = Master_Labels = mongoose.model("master_labels", MasterLabelSchema);
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const NoteSchema = new Schema({
    userId: {
        type: String
    },
    title: {
        type: String
    },
    description: {
        type: String
    },
    url: {
        type: String
        },
    imageUrl: {
        type: String
    },
    currentLabel: {
        type: String
    },
    color: {
        type: String
    },

    flag: {
        type: Number
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    reminderreminderreminder:{
        type: Date
    },
    tags: [String],
    createdDate: {
        type: Date,
        default: Date.now

    }
    
});

module.exports = Note = mongoose.model("notes", NoteSchema);
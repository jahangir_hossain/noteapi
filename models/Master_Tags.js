const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create Schema
const MasterTagSchema = new Schema({
    userId: {
        type: String
    },
    tags: [String]
    
});

module.exports = Master_Tags = mongoose.model("master_tags", MasterTagSchema);
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const NoteServices = require('./routes/api/NoteServices');
const LabelServices = require('./routes/api/LabelServices');
const TagServices = require('./routes/api/TagServices');
const ColorServices = require('./routes/api/ColorServices');

const app = express();

// body-parser middleware
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

//DB config
db = require('./config/keys').mongiURI;

// Connect to mongoDB
mongoose
    .connect(db)
    .then(() => console.log('MongoDB connected'))
    .catch((err) => console.log(err));

const port = process.env.PORT || 5000;

//Use Routes
app.use('/api/note',NoteServices);
app.use('/api/note',LabelServices);
app.use('/api/note',TagServices);
app.use('/api/note',ColorServices);

app.listen(port, () => {
    console.log(`the server is listening at port ${port}`)
});
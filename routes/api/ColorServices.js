const express = require('express');
const router = express.Router();

// Load Input Validation
// const validateTextNoteInput = require('../../validation/textNote');

//load Color model
const Color = require('../../models/Master_Colors');

// @route   GET /api/note/color/test
// @desc    to check color route
// @access  public
router.get('/color/test', (req, res) => res.json({msg: "Color api works", status: "ok", code: 200}));




// @route   GET /api/note/color
// @desc    Get all colors for a specific user
// @access  public
router.get('/color', (req, res) => {

    const errors = {};
    const userId = "userId12121212";
    
    Color.find({userId})
        .then(colors => {
            if(!colors){
                errors.msg = 'there are no color';
                res.status(404).json(errors);
            }

            res.json(colors);
        })
        .catch(err => {
            errors.msg = 'there are no color (exception)';
            res.status(404).json(errors)});
}

);





// @route   POST /api/note/color
// @desc    Create/Add a color/colors for a specific user
// @access  public
router.post('/color', (req, res) => {

    const errors = {};
    const userId = req.body.userId;


    Color.findOne({userId})
         .then(colors => {
             if(!colors){
                 const newColor = new Color({
                     userId,
                     colors: req.body.colors
                 });

                 newColor.save()
                 .then(colors => {
                     const msg = 'new colors created successfully';
                     res.json({colors, msg});

                 })
                 .catch(err => console.log(err));

             } else{

                Color.update({userId: userId},{ $addToSet: { colors:  { $each: req.body.colors} }})
                .then(colors => {
                 if(!colors){
                     errors.msg = 'color: something goes wrong!';
                     res.status(404).json(errors);
                 }else{
                     if(colors.nModified == 1){
                        msg = 'colors added successfully';
                        Color.find({userId})
                             .then(colors => res.json({colors, msg}))
                             .catch(err => {
                                 errors.msg = 'color: Something goes wrong!';
                                 res.status(400).json(errors);
                             })
                            
                     }else if(colors.nModified == 0){
                        errors.msg = 'color already exist';
                        res.status(200).json(errors);
                     }
                 }
                 
             })
             .catch(err => {
                 errors.msg = 'color: something goes wrong! (exception)';
                 console.log(err);
                 res.status(404).json(errors)});
                 

             }
         })
   
}

);




// @route   DELETE /api/note/color/:color
// @desc    Delete a specific color for a specific user by color name 
// @access  public
router.delete('/color/:color', (req, res) => {

    const errors = {};
    const userId = "userId12121212";
    
    Color.update({userId},{ $pull: { colors: req.params.color } })
         .then(color => {
             if(!color){
                errors.msg = "color can not be deleted";
                res.status(404).json(errors)
             }else{
                if(color.nModified == 1){
                    msg = 'color deleted successfully';
                    Color.find({userId})
                             .then(colors => res.json({colors, msg}))
                             .catch(err => {
                                 errors.msg = 'color: Something goes wrong!';
                                 res.status(400).json(errors);
                             })
                    
                 }else if(color.nModified == 0){
                    errors.msg = 'color not found';
                    res.status(200).json(errors);
                 }
             }

         }).catch(err => {
            errors.msg = "color can not be deleted (exception)";
            res.status(404).json(errors)});
   

}

);






module.exports = router;
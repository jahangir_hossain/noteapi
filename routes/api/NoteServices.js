const express = require('express');
const router = express.Router();

// Load Input Validation
const validateTextNoteInput = require('../../validation/textNote');

//load Note model
const Note = require('../../models/Note');


// @route   GET /api/note/test
// @desc    to check note route
// @access  public
router.get('/test', (req, res) => res.json({msg: "Text note works", status: "ok", code: 200}));

// @route   GET   /api/note/list/:userId
// @desc    get all note for a specific user
// @access  public
router.get('/list/:userId', (req, res) => {
    const errors = {};
    
    Note.find({userId: req.params.userId, isDeleted: false})
        .then(notes => {
            if(!notes){
                errors.msg = 'no note exist ';
                res.status(404).json(errors);
            }

            res.status(200).json(notes);
        })
        .catch(err => {
            errors.msg = 'somethis goes wrong! (exception)';
            res.status(404).json(errors)});
});

// @route   GET   /api/note/search/title/:title
// @desc    get all note for a specific user by title name [single tag serach]
// @access  public
router.get('/search/title/:title', (req, res) => {

    const userId = "userId12121212";
    const errors = {};
    
    Note.find({userId: userId, title: req.params.title, isDeleted: false})
        .then(notes => {
            if(!notes){
                errors.msg = 'no note found on this title';
                res.status(404).json();
            }

            res.status(200).json(notes);
        })
        .catch(err => {
            console.log(err);
            errors.msg = 'no note found';
            res.status(404).json(errors)});
});

// @route   GET   /api/note/search/tag/:tag
// @desc    get all note for a specific user by tag [single tag serach]
// @access  public
router.get('/search/tag/:tag', (req, res) => {

    const userId = "userId12121212";
    const errors = {};
    
    Note.find({userId: userId, tags: req.params.tag, isDeleted: false})
        .then(notes => {
            if(!notes){
                errors.msg = 'no note found on this tag';
                res.status(404).json(errors);
            }

            res.status(200).json(notes);
        })
        .catch(err => {
            errors.msg = 'something goes wrong';
            res.status(404).json(errors)});
});


// @route   POST   /api/note/create
// @desc    create a new note
// @access  public
router.post('/create', (req, res) => {

    const {errors, isValid} = validateTextNoteInput(req.body);

    Note.findOne({title: req.body.title})
        .then((note) => {
            if(note){
                errors.msg = "Note already exist!!";
                return res.status(400).json(errors);
            }else{
                const newNote = new Note({

                    userId : req.body.userId,
                    title : req.body.title,
                    description : req.body.description,
                    url : req.body.url,
                    imageUrl : req.body.imageUrl,
                    currentLabel : req.body.currentLabel,
                    tags : req.body.tags,
                    color: req.body.color,
                    flag : req.body.flag,
                    reminder: req.body.reminder
                });

                newNote.save()
                       .then(note => {
                            const msg = "new note created successfully";
                            res.status(200).json({note, msg});

                       })
                       .catch(err => {
                            console.log(err)
                            errors.msg = 'something goes wrong! (exception)';
                            res.status(404).json(errors);
                       });
            }
        })
});

// @route   PUT   /api/note/update/:id
// @desc    update a existing note by id
// @access  public
router.put('/update/:id', (req, res) => {

    const {errors, isValid} = validateTextNoteInput(req.body);


    const updatedNote = {

        userId : req.body.userId,
        title : req.body.title,
        description : req.body.description,
        url : req.body.url,
        imageUrl : req.body.imageUrl,
        currentLabel : req.body.currentLabel,
        tags : req.body.tags,
        color: req.body.color,
        flag : req.body.flag,
        reminder: req.body.reminder
    };

    Note.findOneAndUpdate({_id: req.params.id, isDeleted: false, userId: updatedNote.userId},updatedNote)
        .then(() => {
            Note.find({_id: req.params.id})
            .then(note => {
                const msg = 'note updated successfully';
                res.status(200).json({note, msg})})
            .catch(err =>{
                console.log(err);
                errors.msg = 'something goes wrong';
                res.status(404).json(errors);

            });
            
        })
});


// @route   DELETE /api/note/hardDel/:id
// @desc    to parmanently delete a note from DB
// @access  public
router.delete('/hardDel/:id', (req, res) => {

    const {errors, isValid} = validateTextNoteInput(req.body);

    Note.findOneAndRemove({_id: req.params.id})
        .then(result =>{
            const msg = 'note is deleted from the system';
            res.status(200).json({result, msg});
        }).catch(err => console.log(err));
});

// @route   PUT /api/note/del/:id
// @desc    to softly delete a note. [set the isDeleted flag true]
// @access  public
router.put('/del/:id', (req, res) => {

    const {errors, isValid} = validateTextNoteInput(req.body);

    Note.findOneAndUpdate({_id: req.params.id}, {isDeleted: true})
    .then(() => {
        Note.find({_id: req.params.id})
        .then(note =>{
            const msg = 'note is deleted successfully';
            res.status(200).json({note, msg});})
        .catch(err => console.log(err));
        
    })
        

});


module.exports = router;
const express = require('express');
const router = express.Router();

// Load Input Validation
// const validateTextNoteInput = require('../../validation/textNote');

//load Label model
const Label = require('../../models/Master_Labels');

// @route   GET /api/note/label/test
// @desc    to check label route
// @access  public
router.get('/label/test', (req, res) => res.json({msg: "Label api works", status: "ok", code: 200}));



// @route   GET /api/note/label
// @desc    Get all labels for a specific user
// @access  public
router.get('/label', (req, res) => {

    const errors = {};
    const userId = "userId12121212";
    
    Label.find({userId})
        .then(labels => {
            if(!labels){
                errors.msg = 'there are no Labels';
                res.status(404).json(errors);
            }

            res.status(200).json(labels);
        })
        .catch(err => {
            console.log(err);
            errors.msg = 'there are no Labels';
            res.status(404).json(errors)});
}

);


// @route   POST /api/note/label
// @desc    Create/Add a label for a specific user
// @access  public
router.post('/label', (req, res) => {

    const errors = {};
    const userId = req.body.userId;


    Label.findOne({userId})
         .then(labels => {
             if(!labels){
                 const newLabel = new Label({
                     userId,
                     labels: req.body.labels
                 });

                 newLabel.save()
                 .then(labels => {
                     const msg = 'new label created successfully';
                     res.status(200).json({labels, msg});
                 })
                 .catch(err => console.log(err));

             } else{

                Label.update({userId: userId},{ $addToSet: { labels:  { $each: req.body.labels} }})
                .then(label => {
                 if(!label){
                     errors.msg = 'label: something goes wrong!';
                     res.status(404).json(errors);
                 }else{
                     if(label.nModified == 1){
                        const msg ='label added successfully';
                        Label.find({userId})
                             .then(labels => res.status(200).json({labels, msg}))
                             .catch(err => {
                                 errors.msg = 'label: Something goes wrong!';
                                 res.status(400).json(errors);
                             })
                     }else if(label.nModified == 0){
                        errors.msg = 'label already exist';
                        res.status(200).json(errors);
                     }
                 }
                 
             })
             .catch(err => {
                 console.log(err);
                 errors.msg = 'label: something goes wrong! (exception)';
                 res.status(404).json(errors)});

             }
         })
   
}

);


// @route   DELETE /api/note/label/:label
// @desc    Delete a specific label for a specific user by label name
// @access  public
router.delete('/label/:label', (req, res) => {

    const errors = {};
    const userId = "userId12121212";
    
    Label.update({userId},{ $pull: { labels: req.params.label } })
         .then(label => {
             if(!label){
                errors.msg = "label can not be deleted";
                res.status(404).json(errors);
             }else{
                if(label.nModified == 1){
                    const msg = 'label deleted successfully';
                    Label.find({userId})
                             .then(labels => res.json({labels, msg}))
                             .catch(err => {
                                 errors.msg = 'label: Something goes wrong!';
                                 res.status(400).json(errors);
                             })

                 }else if(label.nModified == 0){
                    errors.msg = 'label did not exist';
                    res.status(200).json(errors);
                 }
             }

         }).catch(err => {
            errors.msg = "label can not be deleted (exception)";
            res.status(404).json(errors)});
    

   

}

);



module.exports = router;
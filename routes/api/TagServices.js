const express = require('express');
const router = express.Router();

// Load Input Validation
// const validateTextNoteInput = require('../../validation/textNote');

//load Tag model
const Tag = require('../../models/Master_Tags');

// @route   GET /api/note/tag/test
// @desc    to check tag route
// @access  public
router.get('/tag/test', (req, res) => res.json({msg: "Tag api works", status: "ok", code: 200}));



// @route   GET /api/note/tag
// @desc    Get all tags for a specific user
// @access  public
router.get('/tag', (req, res) => {

    const errors = {};
    const userId = "userId12121212";
    
    Tag.find({userId})
        .then(tags => {
            if(!tags){
                errors.msg = 'there are no tags';
                res.status(404).json(errors);
            }

            res.json(tags);
        })
        .catch(err => {
            errors.msg = 'there are no tags';
            res.status(404).json(errors)});
}

);



// @route   POST /api/note/tag
// @desc    Create/Add a tag/tags for a specific user
// @access  public
router.post('/tag', (req, res) => {

    const errors = {};
    const userId = req.body.userId;


    Tag.findOne({userId})
         .then(tags => {
             if(!tags){
                 const newTag = new Tag({
                     userId,
                     tags: req.body.tags
                 });

                 newTag.save()
                 .then(tags => {
                    const msg = "new tags created successfully";
                    res.json({tags, msg});})
                 .catch(err => console.log(err));

             } else{

                Tag.update({userId: userId},{ $addToSet: { tags:  { $each: req.body.tags} }})
                .then(tags => {
                 if(!tags){
                     errors.msg = 'tag: something goes wrong!';
                     res.status(404).json(errors);
                 }else{
                     if(tags.nModified == 1){
                        const msg = 'tag added successfully';
                        Tag.find({userId})
                             .then(tags => res.json({tags, msg}))
                             .catch(err => {
                                 console.log(err);
                                 errors.msg = 'tag: Something goes wrong!';
                                 res.status(400).json(errors);
                             })

                     }else if(tags.nModified == 0){
                        errors.msg = 'tag already exist';
                        res.status(200).json(errors);
                     }
                 }
                 
             })
             .catch(err => {
                console.log(err);
                 errors.msg = 'tag: something goes wrong! (exception)';
                 res.status(404).json(errors)});
                 

             }
         })
   
}

);





// @route   DELETE /api/note/tag/:tag
// @desc    Delete a specific tag for a specific user by tag name 
// @access  public
router.delete('/tag/:tag', (req, res) => {

    const errors = {};
    const userId = "userId12121212";
    
    Tag.update({userId},{ $pull: { tags: req.params.tag } })
         .then(tag => {
             if(!tag){
                errors.msg = "tag can not be deleted";
                res.status(404).json(errors)
             }else{
                if(tag.nModified == 1){
                    const msg = 'tag deleted successfully';
                    Tag.find({userId})
                             .then(tags => res.json({tags, msg}))
                             .catch(err => {
                                 console.log(err);
                                 errors.msg = 'tag: Something goes wrong!';
                                 res.status(400).json(errors);
                             })

                 }else if(tag.nModified == 0){
                    errors.msg = 'tag not found successfully';
                    res.status(200).json(errors);
                 }
             }

         }).catch(err => {
             console.log(err);
            errors.msg = "tag can not be deleted (exception)";
            res.status(404).json(errors)});
    

   

}

);




module.exports = router;
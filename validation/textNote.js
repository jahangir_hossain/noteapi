const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = validateTextNoteInput = data => {
    let errors = {};

    data.title = isEmpty(data.title) ?  '' : data.title;
    data.description = isEmpty(data.description) ?  '' : data.description;
    data.url = isEmpty(data.url) ?  '' : data.url;
    data.imageUrl = isEmpty(data.imageUrl) ?  '' : data.imageUrl;

    if(!Validator.isLength(data.title, {min: 2, max: 30})){
        errors.title = "Title must be between 2 to 30 characters";
    }

    if(Validator.isEmpty(data.title)){
        errors.title = "Title Field is required";
    }
    if(!Validator.isLength(data.description, {min: 2, max: 300})){
        errors.description = "Description must be between 2 to 30 characters";
    }

    if(Validator.isEmpty(data.description)){
        errors.description = "Description Field is required";
    }

    if(!Validator.isURL(data.url)){
        errors.url = "invalid URL";
    }

    

   


    return {
        errors,
        isValid: isEmpty(errors)
    }
}
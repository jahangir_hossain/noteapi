import React, { Component } from "react";
import { MDBBtn, MDBCol, MDBContainer, MDBRow } from "mdbreact";
import "./index.css";

//Including Router
import {BrowserRouter as Router, Route} from 'react-router-dom';
import CreateNote from './components/createNote/CreateNote';

class App extends Component {
  render() {
    return (
      <Router>
        <Route exact path="/create" component= { CreateNote }/>
      </Router>
    );
  }
}

export default App;

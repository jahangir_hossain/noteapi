import { MDBContainer, MDBRow, MDBCol, MDBInput, MDBBtn, MDBCard, MDBCardBody, MDBIcon } from 'mdbreact';

import React, { Component,  } from 'react'

class CreateNote extends Component {
    render() {
        return (
            <MDBContainer>
      <MDBRow center className="mt-5">
        <MDBCol md="4">
          <MDBCard>
            <MDBCardBody>
              <form>
                {/* <p className="h4 text-center py-4">Create Note</p> */}
                <div className="grey-text">
                  <MDBInput
                    label="Title"
                    // icon="user"
                    group
                    type="text"
                    validate
                    error="wrong"
                    success="right"
                  />
                  <MDBInput
                    label="Description"
                    // icon="envelope"
                    group
                    type="email"
                    validate
                    error="wrong"
                    success="right"
                  />
                 
                </div>

                

                {/* <div className="text-center py-4 mt-3">
                  <MDBBtn color="cyan" type="submit">
                    Save Note
                  </MDBBtn>
                </div> */}
              </form>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
        )
    }
}




export default CreateNote;